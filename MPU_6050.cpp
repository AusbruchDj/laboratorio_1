/* mbed Microcontroller Library
 * Copyright (c) 2019 ARM Limited
 * SPDX-License-Identifier: Apache-2.0
 */
#include "mbed.h"
#define MPU6050_address 0xD0
//ESCALAS DEL GIROSCOPIO
#define GFS_SEL_250 0x00
#define GFS_SEL_500 0x08
#define GFS_SEL_1000 0x10
#define GFS_SEL_2000 0x18
//ESCALAS DEL ACELERÓMETRO
#define AFS_SEL_2G 0X00
#define AFS_SEL_4G 0X08
#define AFS_SEL_8G 0X10
#define AFS_SEL_16G 0X18
//ESCALAS DE CONVERSIÓN DEL ACELERÓMETRO,GIROSCOPIO Y TEMPERATURA
#define SENSITIVITY_ACCEL 2.0/32768.0
#define SENSITIVITY_GYRO 250.0/32768.0
#define SENSITIVITY_TEMP 340.0
#define TEMP_OFFET 20

//DECLARACIÓN DE LAS VARIABLES 
int16_t raw_Accelx,raw_Accely,raw_Accelz;
int16_t raw_Gyrox,raw_Gyroy,raw_Gyroz;
int16_t raw_Temp;
//SALIDAS CALIBRADAS 
float Accelx,Accely,Accelz;
float Gyrox,Gyroy,Gyroz;
float Temp;

char reg[2];
char data[1];
char GyrAcel[14];
float buffer[500][8];
float timer=0;
Timer t;

Serial pc(SERIAL_TX,SERIAL_RX);
I2C i2c(PB_9,PB_8);


int main(){
    reg[0]=0x6B;
    reg[1]=0x00;
    i2c.write(MPU6050_address,reg,2);
    pc.printf("PRUEBA DE CONEXION PARA EL ACELEROMETRO Y EL GIROSCOPIO \n\r");
    pc.printf("1.VERIFICANDO LA CONEXION CON EL MPU6050...\n\r");
    reg[0]=0x75;
    i2c.write(MPU6050_address,reg,1);
    i2c.read(MPU6050_address,data,1);
    if (data[0]!=0x68){
        pc.printf("ERROR DE COMUNICACION Y CONEXION CON LA MPU6050 \n\r");
        pc.printf("%#x \n\r",data[0]);
        while(1);
        }
    else{
        pc.printf("CONEXION EXITOSA CON LA  MPU6050 \n\r");
        }
    wait(0.1);
    //CONFIGURACIÓN DEL GIROSCOPIO A UNA ESCALA CON UN RANGO DE 250 deg/s
    reg[0]=0x1B;
    reg[1]=0x00;
    i2c.write(MPU6050_address,reg,2);
    //CONFIGURACIÓN DEL ACELERÓMETRO A UNA ESCALA CON UN RANGO DE 2 g
     reg[0]=0x1C;
    reg[1]=0x00;
    i2c.write(MPU6050_address,reg,2);
    wait(0.1);
    while (1){
        if(pc.getc()=='A'|| pc.getc()=='a'){
            for(int i=0;i<=499;i++){
                reg[0]=0x3B;
                i2c.write(MPU6050_address,reg,1);
                i2c.read(MPU6050_address,GyrAcel,14);
                t.reset();
                t.start();
                raw_Accelx= GyrAcel[0]<<8 |GyrAcel[1];
                raw_Accely= GyrAcel[2]<<8 |GyrAcel[3];
                raw_Accelz= GyrAcel[4]<<8 |GyrAcel[5];
                raw_Gyrox=  GyrAcel[6]<<8 |GyrAcel[7];
                raw_Gyroy=  GyrAcel[8]<<8 |GyrAcel[9]; 
                raw_Gyroz=  GyrAcel[10]<<8 |GyrAcel[11];
                raw_Temp=   GyrAcel[12]<<8 |GyrAcel[13];
                
                Accelx= raw_Accelx*SENSITIVITY_ACCEL;
                Accely= raw_Accely*SENSITIVITY_ACCEL;
                Accelz= raw_Accelz*SENSITIVITY_ACCEL;
                Gyrox=  raw_Gyrox*SENSITIVITY_GYRO;
                Gyroy=  raw_Gyroy*SENSITIVITY_GYRO;
                Gyroz=  raw_Gyroz*SENSITIVITY_GYRO;
                Temp=   (raw_Temp/SENSITIVITY_TEMP)+20;
                wait_ms(9);
                wait_us(962);
                t.stop();
                timer=t.read();
                pc.printf("EL TIEMPO ES DE %F SEGUNDOS \r",timer);
                pc.printf("%d:%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f\n\r",i+1,Accelx,Accely,Accelz,Gyrox,Gyroy,Gyroz,Temp);
            }
        }

    }

}

